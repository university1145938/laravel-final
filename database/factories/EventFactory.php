<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $startDate = Carbon::now();
        $endDate = $startDate->copy()->addDays(random_int(2, 5));
        return [
            'start_time' => $startDate,
            'end_time' => $endDate,
            'name' => fake()->title()
        ];
    }
}
