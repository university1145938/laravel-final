<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Comment;
use App\Models\Event;
use App\Models\Post;
use App\Models\User;
use Database\Factories\PostFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $users = User::factory(10)->has(
            Post::factory(3)->has(
                Comment::factory(5)
            )
        )->create();


        $events = Event::factory(5)->create();

        foreach($events as $event) {
            $randomUsers = $users->shuffle()->take(random_int(1, 3));
            foreach($randomUsers as $user) {
                $user->events()->attach($event);
            }
        }
    }
}
