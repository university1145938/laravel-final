<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post("/register", RegistrationController::class);

Route::prefix('/post')->group(function () {
    Route::post("/", [PostController::class, 'create']);
    Route::get("/", [PostController::class, 'index']);
    Route::get("/{post}", [PostController::class, 'show']);
});

Route::prefix('/user')->group(function () {
    Route::get("/{user}", [UserController::class, 'show']);
    Route::get("/{user}/events", [UserController::class, 'events']);
});

Route::prefix('/comment')->group(function () {
    Route::post("/", [CommentController::class, 'create']);
    Route::delete("/{comment}", [CommentController::class, 'delete']);
});

Route::prefix('/event')->group(function () {
    Route::post("/{event}", [EventController::class, 'attend']);
    Route::get("/{event}", [EventController::class, 'show']);
});
