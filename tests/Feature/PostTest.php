<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_post_can_be_created_with_valid_data_and_photos()
    {
        Storage::fake();
    
        $user = User::factory()->create();
    
        $response = $this->postJson('/api/post', [
            'user_id' => $user->id,
            'title' => 'Valid Title',
            'body' => 'This is a valid body of the post, with enough length.',
            'photos' => [
                UploadedFile::fake()->image('photo1.jpg'),
                UploadedFile::fake()->image('photo2.jpg')
            ]
        ]);
    
        $response->assertStatus(200);
        $response->assertJson(['message' => 'success']);
    
        $this->assertDatabaseHas('posts', [
            'user_id' => $user->id,
            'title' => 'Valid Title',
            'body' => 'This is a valid body of the post, with enough length.',
        ]);
    
        $post = Post::where('title', 'Valid Title')->first();
        $this->assertCount(2, $post->medias);
        foreach ($post->medias as $media) {
            Storage::assertExists($media->path);
        }
    }

    public function test_post_creation_fails_if_required_fields_are_missing()
    {
        $response = $this->postJson('/api/post', []);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['user_id', 'title', 'body']);
    }

    public function test_post_creation_fails_if_user_id_is_invalid()
    {
        $response = $this->postJson('/api/post', [
            'user_id' => 999,
            'title' => 'Valid Title',
            'body' => 'Valid Body'
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('user_id');
    }

    public function test_post_creation_fails_if_photos_are_invalid()
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/post', [
            'user_id' => $user->id,
            'title' => 'Valid Title',
            'body' => 'Valid Body',
            'photos' => [
                'not_a_file',
                UploadedFile::fake()->create('document.pdf') // Invalid file type
            ]
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['photos.0', 'photos.1']);
    }


    public function test_posts_are_returned_with_correct_structure()
    {
        $post = Post::factory()->create();
        Comment::factory()->for($post)->for(User::factory())->create();

        $response = $this->getJson('/api/post');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                "*" => [
                    'id',
                    'title',
                    'body',
                    'comments' => [
                        '*' => [
                            'id',
                            'body',
                            'user' => [
                                'id',
                                'name',
                                'email',
                                'avatar',
                            ]
                        ]
                    ],
                    'created_at'
                ]
            ]
        ]);
    }

    public function test_posts_are_returned_with_correct_content()
    {
        $post = Post::factory()->create([
            'title' => 'Test Post',
            'body' => 'Test Body'
        ]);
        
        $comment = Comment::factory()->for($post)->for(User::factory())->create();

        $response = $this->getJson('/api/post');

        $response->assertJson([
            "data" => [
                "0" =>  [
                    'id' => $post->id,
                    'title' => 'Test Post',
                    'body' => 'Test Body',
                    'comments' => [
                        "0" => [
                            'id' => $comment->id,
                            'body' => $comment->body,
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function test_single_post_is_returned_with_correct_content()
    {
        $post = Post::factory()->create([
            'title' => 'Test Post',
            'body' => 'Test Body'
        ]);
        
        $comment = Comment::factory()->for($post)->for(User::factory())->create();

        $response = $this->getJson("/api/post/$post->id");

        $response->assertJson([
            'id' => $post->id,
            'title' => 'Test Post',
            'body' => 'Test Body',
            'comments' => [
                "0" => [
                    'id' => $comment->id,
                    'body' => $comment->body,
                ]
            ]
        ]);
    }

    public function test_post_not_found()
    {
        $response = $this->getJson("/api/post/99");
        $response->assertStatus(404);
    }

}
