<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use RefreshDatabase;

    public function test_user_is_returned_with_correct_content()
    {
        $user = User::factory()->create([
            'name' => 'john doe',
        ]);


        $response = $this->getJson("/api/user/$user->id");
        $response->assertJson([
            'data' => [
                'id' => $user->id,
                'name' => 'john doe',
                'email' => $user->email,
            ]
        ]);
    }

    public function test_user_not_found()
    {
        $response = $this->getJson("/api/user/99");
        $response->assertStatus(404);
    }
}
