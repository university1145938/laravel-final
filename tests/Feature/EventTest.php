<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_attend_event(): void
    {
        $user = User::factory()->create();
        $event = Event::factory()->create();

        $response = $this->postJson("/api/event/$event->id", [
            'user_id' => $user->id
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('event_user', ['user_id' => $user->id, 'event_id' => $event->id]);
    }

    public function test_can_not_attend_event_without_user_id(): void
    {
        $event = Event::factory()->create();

        $response = $this->postJson("/api/event/$event->id");
        $response->assertStatus(422);

        $response->assertJsonValidationErrors('user_id');
    }

    public function test_can_not_attend_event_with_invalid_user_id(): void
    {
        $event = Event::factory()->create();

        $response = $this->postJson("/api/event/$event->id", [
            'user_id' => 999
        ]);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('user_id');
    }

    public function test_event_is_returned_with_correct_content()
    {
        $event = Event::factory()->create([
            'name' => 'random event',
        ]);

        $user = User::factory()->create(
            [
                'name' => 'john doe'
            ]
        );

        $event->users()->attach($user);

        $response = $this->getJson("/api/event/$event->id");
        $response->assertJson([
            'data' => [
                'name' => 'random event',
                'users' => [
                    "0" => [
                        'id' => $user->id,
                        'name' => $user->name,
                    ]
                ]
            ]
        ]);
    }
}
