<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use RefreshDatabase;

    public function test_a_user_can_be_registered()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'password' => 'secret123',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'success']);

        $this->assertDatabaseHas('users', [
            'email' => 'johndoe@example.com',
            'name' => 'John Doe',
        ]);


        $user = User::where('email', 'johndoe@example.com')->first();
        Storage::assertExists($user->medias->first()->path);
    }

    public function test_registration_fails_if_name_is_missing()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'email' => 'test@example.com',
            'password' => 'password123',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');
    }

    public function test_registration_fails_if_email_is_missing()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'password' => 'password123',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }

    public function test_registration_fails_if_password_is_missing()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'email' => 'test@example.com',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('password');
    }

    public function test_registration_fails_if_email_is_invalid()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'email' => 'invalid-email',
            'password' => 'password123',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('email');
    }


    public function test_registration_fails_if_password_is_too_short()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'email' => 'test@example.com',
            'password' => 'pass',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('password');
    }

    public function test_registration_fails_if_avatar_is_not_an_image()
    {
        Storage::fake();

        $response = $this->postJson('/api/register', [
            'name' => 'John Doe',
            'email' => 'test@example.com',
            'password' => 'password123',
            'avatar' => UploadedFile::fake()->create('document.pdf')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('avatar');
    }
}
