<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Database\Factories\CommentFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    public function test_comment_creation_fails_if_body_is_invalid()
    {
        $user = User::factory()->create();
        $post = Post::factory()->create();

        $response = $this->postJson('/api/comment', [
            'user_id' => $user->id,
            'post_id' => $post->id,
            'body' => ''
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors('body');
    }

    public function test_comment_creation_fails_if_user_id_and_post_id_are_invalid()
    {
        $response = $this->postJson('/api/comment', [
            'user_id' => 1,
            'post_id' => 1,
            'body' => 'valid body'
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors( ['user_id', 'post_id'] );
    }

    public function test_can_delete_a_comment()
    {

        $comment = Comment::factory()->create();
        $response = $this->delete("/api/comment/$comment->id");

        $response->assertStatus(200);
        $this->assertDatabaseCount('comments', 0);
    }
}
