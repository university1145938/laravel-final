<?php

namespace Tests\Unit;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Tests\TestCase;
use App\Services\ThreadService;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     */

    use RefreshDatabase;

    private function createComments($comments, $userId, $postId, $parentId = null)
    {

        foreach($comments as $comment) {
            $createdComment = Comment::create(
                [
                    'body' => $comment['body'],
                    'parent_id' => $parentId,
                    'user_id' => $userId,
                    'post_id' => $postId
                ]
            );

            $this->createComments($comment['replies'], $userId, $postId, $createdComment->id);
        }
    }


    private function isSameTree(&$node1, &$node2)
    {
        if($node1['body'] != $node2['body']) {
            return false;
        }

        if(count($node1['replies']) != count($node2['replies'])) {
            return false;
        }

        usort($node1['replies'], function ($elem1, $elem2) {
            return $elem1 > $elem2;
        });

        usort($node2['replies'], function ($elem1, $elem2) {
            return $elem1 > $elem2;
        });

        for($i = 0; $i < count($node2['replies']); $i++) {
            $result = $this->isSameTree(
                $node1['replies'][$i],
                $node2['replies'][$i]
            );

            if(!$result) {
                return false;
            }
        }

        return true;
    }

    public function test_thread_service(): void
    {

        // all comments' bodies should be distinct,
        // or else there will be no way to compare result with original structure

        $commentStructure = [
            [
                'body' => 'parent 1',
                'replies' => [
                    [
                        'body' => 'child 1',
                        'replies' => [
                            [
                                'body' => 'child 5',
                                'replies' => []
                            ],
                            [
                                'body' => 'child 6',
                                'replies' => []
                            ],
                            ]
                    ],
                    [
                        'body' => 'child 2',
                        'replies' => []
                    ],
                ]
            ],
            [
                'body' => 'parent 2',
                'replies' => [
                    [
                        'body' => 'child 3',
                        'replies' => [
                            [
                                'body' => 'child 7',
                                'replies' => [
                                    [
                                        'body' => 'child 9',
                                        'replies' => []
                                    ]
                                ]
                            ],
                            [
                                'body' => 'child 8',
                                'replies' => []
                            ],
                        ]
                    ],
                    [
                        'body' => 'child 4',
                        'replies' => []
                    ],
                ]
            ],
        ];

        $user = User::factory()->create();
        $post = Post::factory()->create();
        $this->createComments($commentStructure, $user->id, $post->id);


        $service = new ThreadService();
        $threadTree = $service->buildThreadTree($post);

        $this->assertTrue(count($threadTree) == count($commentStructure));

        usort($threadTree, function ($elem1, $elem2) {
            return $elem1 > $elem2;
        });

        usort($commentStructure, function ($elem1, $elem2) {
            return $elem1 > $elem2;
        });

        for($i = 0; $i < count($commentStructure); $i++) {

            $result = $this->isSameTree(
                $commentStructure[$i],
                $threadTree[$i]
            );

            $this->assertTrue($result);
        }


        // adding comment in database so it wont match with comment structure defined above

        Comment::factory()->create(
            [
                'body' => 'jemala',
                'post_id' => $post->id,
                'user_id' => $user->id,
                "parent_id" => Comment::first()->id
            ]
        );
        $post->load('comments');
        $service = new ThreadService();
        $threadTree = $service->buildThreadTree($post);

        $gotAtLeastOneFalseResult = false;

        for($i = 0; $i < count($commentStructure); $i++) {

            $result = $this->isSameTree(
                $commentStructure[$i],
                $threadTree[$i]
            );

            if(!$result) {
                $gotAtLeastOneFalseResult = true;
            }
        }

        $this->assertTrue($gotAtLeastOneFalseResult);
    }
}
