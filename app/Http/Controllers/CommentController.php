<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\DeleteCommentRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create(CreateCommentRequest $request)
    {
        Comment::create(
            $request->validated()
        );

        return ['message' => 'success'];
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
        return ['message' => 'success'];
    }
}
