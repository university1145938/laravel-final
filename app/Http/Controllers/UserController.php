<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(User $user)
    {
        $user->load('medias', 'posts');
        return UserResource::make($user);
    }

    public function events(User $user)
    {
        $user->load('events');
        return  UserResource::make($user);
    }
}
