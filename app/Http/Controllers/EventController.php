<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttendEventRequest;
use App\Http\Resources\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function show(Event $event)
    {
        $event->load("users");
        return EventResource::make($event);
    }

    public function attend(Event $event, AttendEventRequest $request)
    {
        $event->users()->attach($request->user_id);
        return ['message' => 'success'];
    }
}
