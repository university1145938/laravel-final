<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\Media;
use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function __invoke(RegistrationRequest $request)
    {
        $user = User::create(
            [
                "name" => $request->name,
                "email" => $request->email,
                "password" => bcrypt($request->password)
            ]
        );

        $path = $request->avatar->store('avatars');

        $user->medias()->create([
            'path' => $path
        ]);

        return ['message' => 'success'];
    }
}
