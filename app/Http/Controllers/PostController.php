<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Services\ThreadService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create(PostRequest $request)
    {
        $post = Post::create([
            "user_id" => $request->user_id,
            "title" => $request->title,
            "body" => $request->body,
        ]);

        if($request->photos) {
            foreach($request->photos as $photo) {
                $path = $photo->store('posts');
                $post->medias()->create([
                    'path' => $path
                ]);
            }
        }

        return ['message' => 'success'];
    }

    public function show(Post $post)
    {
        $post->load('comments.user.medias');

        $threadService = new ThreadService();
        $threadTree = $threadService->buildThreadTree($post);

        $post = $post->toArray();
        $post['comments'] = $threadTree;
        return $post;
    }

    public function index()
    {
        return PostResource::collection(Post::with('comments.user.medias')->get());
    }
}
