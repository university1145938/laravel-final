<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => trim($this->name),
            'email' => trim($this->email),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required','string', 'min:5', 'max:55'],
            'email' => ['required','email', 'unique:users,email'],
            'password' => ['required','string', 'min:5', 'max:55'],
            'avatar' => ['required','file','max:10000', 'mimes:png,jpg'],
        ];
    }


    public function messages(): array
    {
        return [
            'email.unique' => 'user with provided email already exists',
            'avatar.mimes' => 'only png and jpg formats are allowed',
        ];
    }
}
