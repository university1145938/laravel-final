<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AttendEventRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */

    public function rules(): array
    {
        $eventAttendeesIds = $this->event->users->pluck('id')->toArray();
        return [
            'user_id' => ['required', 'integer', 'exists:users,id', Rule::notIn($eventAttendeesIds)]
        ];
    }
}
