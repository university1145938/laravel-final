<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'title' => trim($this->title),
        ]);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'exists:users,id'],
            'title' => ['required', 'string', 'min:5', 'max:55'],
            'body' => ['required', 'string', 'min:10', 'max:50000'],
            'photos' => ['array'],
            'photos.*' => ['file', 'mimes:png,jpg', 'max:20000']
        ];
    }


    public function messages(): array
    {
        return [
            'user_id.exists' => 'user with provided id does not exist',
            'photos.*.mimes' => 'only png and jpg formats are allowed',
        ];
    }
}
