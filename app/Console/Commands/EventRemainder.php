<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Notifications\EventNotification;
use Illuminate\Console\Command;

class EventRemainder extends Command
{

    protected $signature = 'app:event-remainder';

    protected $description = 'notifies event attendees when event is getting close';

    public function handle()
    {
        $upcomingEventAttendees = User::whereHas('events', function($eventQuery){
            $eventQuery->where('start_time', '>', now())
            ->where('start_time', '<', now()->addDays(2));
        })
        ->get();

        foreach($upcomingEventAttendees as $attendee){
            $attendee->notify(new EventNotification($attendee));
        }
    }
}
