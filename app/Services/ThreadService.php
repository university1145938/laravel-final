<?php

namespace App\Services;

use App\Http\Resources\CommentResource;
use App\Models\Post;

class ThreadService
{
    private function dfs(&$adjList, &$comment)
    {
        $id = $comment['id'];

        if(
            isset($adjList[$id])
        ) {
            $comment['replies'] = $adjList[$id];
        } else {
            $comment['replies'] = [];
        }

        foreach($comment['replies'] as &$reply) {
            $this->dfs($adjList, $reply);
        }
    }

    public function buildThreadTree(Post $post)
    {
        $comments = $post->comments->toArray();
        $topLayer = [];
        $parentIdCommentMap = [];

        foreach($comments as $comment) {
            $parentId = $comment['parent_id'];
            if(
                !isset($parentIdCommentMap[$parentId])
            ) {
                $parentIdCommentMap[$parentId] = [];
            }
            $parentIdCommentMap[$parentId][] = $comment;
        }

        $topLayer = $parentIdCommentMap[null];
        foreach($topLayer as &$node) {
            $this->dfs($parentIdCommentMap, $node);
        }

        return $topLayer;
    }
}
